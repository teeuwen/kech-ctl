/*
 *
 * kech-ctl
 * menu.c
 *
 * Copyright (C) 2018 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "menu.h"

#include <menu.h>
#include <ncurses.h>

#include <stdlib.h>
#include <string.h>

static void quit();

static struct menu menu_main[4];

static struct menu menu_useradd[] = {
	{ "Back",		(struct menu **) &menu_main,	NULL,	0 },
	{ NULL,			NULL,				NULL,	0 }
};

static struct menu menu_usermod[] = {
	{ "Back",		(struct menu **) &menu_main,	NULL,	0 },
	{ NULL,			NULL,				NULL,	0 }
};

static struct menu menu_main[] = {
	{ "Add user",		(struct menu **) &menu_useradd,	NULL,	0 },
	{ "Modify user",	(struct menu **) &menu_usermod,	NULL,	0 },
	{ "Quit",		NULL,				&quit,	0 },
	{ NULL,			NULL,				NULL,	0 }
};

struct menu *curmenu;
static WINDOW *win;
static MENU *menu;
static ITEM **items;

static void shadow_draw(int x, int y, int w, int h)
{
	int i;

	attron(COLOR_PAIR(2));

	for (i = x + 2; i <= x + w; i++) {
		move(y + h, i);
		addch(' ');
	}

	for (i = y + 1; i <= y + h; i++) {
		move(i, x + w);
		addch(' ');

		move(i, x + w + 1);
		addch(' ');
	}

	attroff(COLOR_PAIR(2));
}

static void menu_alloc(struct menu *m, int n)
{
	int i, x, y, w, h;

	w = COLS / 2;
	h = LINES / 2;

	/* Center the window */
	x = (COLS - w) / 2;
	y = (LINES - h) / 2;

	/* Create the window and draw it's shadow */
	win = newwin(h, w, y, x); /* TODO Error checking */
	wbkgd(win, A_NORMAL | COLOR_PAIR(1) | ' ');
	shadow_draw(x, y, w, h);

	/* Allocate menu items */
	items = realloc(items, n * sizeof(ITEM *));
	memset(items, 0, n * sizeof(ITEM *));

	for (i = 0; i < n; i++)
		items[i] = new_item(m[i].name, NULL);

	/* Allocate menu */
	menu = new_menu(items);
	set_menu_win(menu, win);
	set_menu_sub(menu, derwin(win, 6, 38, 3, 1));

	/* Set menu item indicator */
	set_menu_mark(menu, "");

	/* Show the menu */
	post_menu(menu);

	curmenu = m;
}

static void menu_free(void)
{
	unsigned int i;

	unpost_menu(menu);
	free_menu(menu);

	for (i = 0; i < sizeof(curmenu) / sizeof(*curmenu); i++)
		free_item(items[i]);

	endwin();
}

static void quit()
{
	menu_free();
	free(items);
	fini();

	exit(0);
}

void run(void)
{
	char c;

	refresh();

	menu_alloc(menu_main, sizeof(menu_main) / sizeof(menu_main[0]));
	wrefresh(win);

	while ((c = getch()) != 'q') {
		switch (c) {
		case 'e':
			break;
		}
	}

	quit();
}
