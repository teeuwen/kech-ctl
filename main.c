/*
 *
 * kech-ctl
 * main.c
 *
 * Copyright (C) 2018 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include <mysql/mysql.h>

#include <ncurses.h>

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "menu.h"

static char *sql_host, *sql_db, *sql_user, *sql_pass;
static uint16_t sql_port;

static void usage(char *prog)
{
	printf("Usage: %s [OPTION...]\n\n%s", prog,
			"  -I IP ADDRESS        MySQL database host\n"
			"  -P PORT NUMBER       MySQL database port\n"
			"  -d DB                MySQL database name\n"
			"  -u USER              MySQL database username\n"
			"  -a PASSWORD          MySQL database password\n"
			"  -h                   show this help message\n"
			);
}

void fini(void)
{
	endwin();
	free(sql_host);
	free(sql_db);
	free(sql_user);
	free(sql_pass);
}

int main(int argc, char **argv)
{
	char *s;
	int c;

	/* Parse arguments */
	while ((c = getopt(argc, argv, "I:P:d:u:a:h")) != -1) {
		switch (c) {
		/* MySQL database host */
		case 'I':
			if (!(sql_host = malloc(strlen(optarg) + 1)))
				goto err;
			strcpy(sql_host, optarg);
			break;
		/* MySQL database port */
		case 'P':
			sql_port = strtol(optarg, NULL, 10);
			break;
		/* MySQL database database name */
		case 'd':
			if (!(sql_db = malloc(strlen(optarg) + 1)))
				goto err;
			strcpy(sql_db, optarg);
			break;
		/* MySQL database username */
		case 'u':
			if (!(sql_user = malloc(strlen(optarg) + 1)))
				goto err;
			strcpy(sql_user, optarg);
			break;
		/* MySQL database password */
		case 'a':
			if (!(sql_pass = malloc(strlen(optarg) + 1)))
				goto err;
			strcpy(sql_pass, optarg);
			break;
		/* Usage */
		case 'h':
			usage(argv[0]);

			return 0;
		case '?':
		default:
			usage(argv[0]);
			goto err;
		}
	}

	if (!sql_host) {
		s = "localhost";
		sql_host = malloc(strlen(s) + 1);
		strcpy(sql_host, s);
	}

	if (!sql_db) {
		s = "kech";
		sql_db = malloc(strlen(s) + 1);
		strcpy(sql_db, s);
	}

	if (!sql_user) {
		s = "root";
		sql_user = malloc(strlen(s) + 1);
		strcpy(sql_user, s);
	}

	if (!sql_pass) {
		s = "";
		sql_pass = malloc(strlen(s) + 1);
		strcpy(sql_pass, s);
	}

	/* Initialize ncurses */
	initscr();
	cbreak();
	noecho();
	curs_set(0);

	/* Set color pairs */
	start_color();
	assume_default_colors(COLOR_BLACK, COLOR_BLUE);
	init_pair(1, COLOR_BLACK, COLOR_WHITE);
	init_pair(2, COLOR_BLACK, COLOR_BLACK);

	run();

	fini();
	return 0;

err:
	fini();
	return 1;
}
