#
# Makefile
#

CC		:= gcc

PKGLIST		= mariadb menu ncurses

MAKEFLAGS	:= -s

CFLAGS		:= -Wall -Wextra -Wpedantic -std=c99 -D_XOPEN_SOURCE=700 `pkg-config --cflags $(PKGLIST)` -g -O0 #-s -Os
LDFLAGS		:= `pkg-config --libs $(PKGLIST)`

kech-ctl-o	= main.o menu.o

all: kech-ctl

clean:
	[ -f kech-ctl ] && echo -e "  RM      kech-ctl" || true
	rm -f kech-ctl
	find . -type f -name '*.o' -delete -exec sh -c "echo '  RM      {}'" \;

%.o: %.c
	echo -e "  CC      $@"
	$(CC) $(CFLAGS) -c $< -o $@

kech-ctl: $(kech-ctl-o)
	echo -e "  CC      $@"
	$(CC) -o $@ $(kech-ctl-o) $(LDFLAGS)
